@extends('adminlte.master')

@section('content')
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
              @if(session('success'))
                 <div class ="alert alert-success">
                    {{session('success')}}
                 </div>
              @endif
                <a href="/cast/create" class = "btn btn-primary mb-2">Create New Cast</a>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">No.</th>
                      <th>Nama</th>
                      <th>Umur</th>
                      <th>Bio</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($cast as $key => $keycast)
                    <tr>
                    <td>{{$key + 1 }}</td>
                    <td>{{$keycast -> nama}}</td>
                    <td>{{$keycast -> umur}}</td>
                    <td>{{$keycast -> bio}}</td>
                    <td style ="display : flex">
                        <a href="/cast/{{$keycast->id}}" class="btn btn-info btn-sm">Show</a>
                        <a href="/cast/{{$keycast->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                        <form action="/cast/{{$keycast->id}}" method = "POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" value="delete" class ="btn btn-danger btn-sm">
                        </form>
                    </td>
                    </tr>
                    @empty
                    <tr>
                    <td colspan="4" align="center">Tidak Ada Cast</td>
                    </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div> -->
            </div>
@endsection