@extends('adminlte.master')

@section('content')
<div class ="ml-3 mt-3">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create New Cast</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role ="form" action="/cast" method ="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama">
                    @error('nama')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" class="form-control" id="umur" name="umur" placeholder="Masukkan Umur">
                    @error('umur')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="bio">Biodata</label>
                    <input type="text" class="form-control" id="bio" name="bio" placeholder="Masukkan Biodata">
                    @error('bio')
                      <div class = "alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>                  
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Create</button>
                </div>
              </form>
            </div>
</div>
@endsection