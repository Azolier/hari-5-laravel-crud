<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });

Route::get('/', function () {
    return view('adminlte.master');
});



Route::get('/test', function () {
    return "OK";
});

Route::get('/form', 'App\Http\Controllers\RegisterController@form');

Route::get('/sapa','App\Http\Controllers\RegisterController@sapa');
Route::post('/welcome','App\Http\Controllers\RegisterController@sapa_post');

// Route::get('/master', function () {
//     return view('adminlte.master');
// });

Route::get('/table', function () {
    return view('adminlte.table.table');
});

Route::get('/table', function () {
    return view('adminlte.table.table');
});
Route::get('/data-table', function () {
    return view('adminlte.table.datatable');
});
Route::get('/cast/create', 'App\Http\Controllers\CastController@create');
Route::post('/cast','App\Http\Controllers\CastController@store');
Route::get('/cast', 'App\Http\Controllers\CastController@index');
Route::get('/cast/{id}','App\Http\Controllers\CastController@show');
Route::get('/cast/{id}/edit','App\Http\Controllers\CastController@edit');
Route::put('/cast/{id}','App\Http\Controllers\CastController@update');
Route::delete('/cast/{id}','App\Http\Controllers\CastController@destroy');

